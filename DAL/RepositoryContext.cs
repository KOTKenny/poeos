﻿using DAL.Configuration;
using DAL.Models;
using DAL.Models.Procedures;
using Microsoft.EntityFrameworkCore;
using System;

namespace DAL
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new SensorConfiguration());
            modelBuilder.ApplyConfiguration(new IndicationConfiguration());
            modelBuilder.ApplyConfiguration(new RegionConfiguration());
        }

        public DbSet<Sensor> Sensors { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Indication> Indices { get; set; }
    }
}

﻿using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IRepositoryManager
    {
        public ISensorRepository SensorRepository { get; }
        public IRegionRepository RegionRepository { get; }
        public IIndicationRepository IndicationRepository { get; }
    }
}

﻿using DAL.Models;
using DAL.Models.Procedures;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class IndicationRepository : RepositoryBase<Indication>, IIndicationRepository
    {
        public IndicationRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }

        public IEnumerable<Indication> GetAllIndications(bool trackChanges) =>
            FindAll(trackChanges);

        public Indication GetIndicationById(int indicationId, bool trackChanges) =>
            FindByCondition(x => x.Id.Equals(indicationId), trackChanges)
                .SingleOrDefault();
    }
}

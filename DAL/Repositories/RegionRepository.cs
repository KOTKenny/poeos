﻿using DAL.Models;
using DAL.Models.Procedures;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class RegionRepository : RepositoryBase<Region>, IRegionRepository
    {
        public RegionRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }

        public IEnumerable<Region> GetAllRegions(bool trackChanges) =>
            FindAll(trackChanges);

        public Region GetRegionById(int regionId, bool trackChanges) =>
            FindByCondition(x => x.Id.Equals(regionId), trackChanges)
                .SingleOrDefault();
    }
}

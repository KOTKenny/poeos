﻿using DAL.Models;
using DAL.Models.Procedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Interfaces
{
    public interface ISensorRepository
    {
        IEnumerable<Sensor> GetAllSensors(bool trackChanges);
        Sensor GetSensorById(int sensorId, bool trackChanges);
    }
}

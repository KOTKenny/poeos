﻿using DAL.Models;
using DAL.Models.Procedures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Interfaces
{
    public interface IIndicationRepository
    {
        IEnumerable<Indication> GetAllIndications(bool trackChanges);
        Indication GetIndicationById(int indicationId, bool trackChanges);
    }
}

﻿using DAL.Models;
using DAL.Models.Procedures;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class SensorRepository : RepositoryBase<Sensor>, ISensorRepository
    {
        public SensorRepository(RepositoryContext repositoryContext) : base(repositoryContext)
        {

        }
        public IEnumerable<Sensor> GetAllSensors(bool trackChanges) =>
            FindAll(trackChanges);

        public Sensor GetSensorById(int sensorId, bool trackChanges) =>
            FindByCondition(x => x.Id.Equals(sensorId), trackChanges)
                .SingleOrDefault();
    }
}

﻿using DAL.Repositories;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class RepositoryManager : IRepositoryManager
    {
        private RepositoryContext _repositoryContext;
        private ISensorRepository _sensorRepository;
        private IRegionRepository _regionRepository;
        private IIndicationRepository _indicationRepository;

        public RepositoryManager(RepositoryContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }
        public ISensorRepository SensorRepository
        {
            get
            {
                if(_sensorRepository == null)
                {
                    _sensorRepository = new SensorRepository(_repositoryContext);
                }

                return _sensorRepository;
            }
        }

        public IRegionRepository RegionRepository
        {
            get
            {
                if (_regionRepository == null)
                {
                    _regionRepository = new RegionRepository(_repositoryContext);
                }

                return _regionRepository;
            }
        }

        public IIndicationRepository IndicationRepository
        {
            get
            {
                if (_indicationRepository == null)
                {
                    _indicationRepository = new IndicationRepository(_repositoryContext);
                }

                return _indicationRepository;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models.Procedures
{
    public class SensorLocation
    {
        public int Id { get; set; }
        public int Longitude { get; set; }
        public int Latitude { get; set; }
    }
}

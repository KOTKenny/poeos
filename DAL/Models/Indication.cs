﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Indication
    {
        public int Id { get; set; }
        public int SensorId { get; set; }
        public Sensor Sensor { get; set; }
        [Column(TypeName = "Date")]
        public DateTime CreateDate { get; set; }
        public TimeSpan CreateTime { get; set; }
        public int Temperature { get; set; }
    }
}

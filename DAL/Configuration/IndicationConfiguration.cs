﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Configuration
{
    public class IndicationConfiguration : IEntityTypeConfiguration<Indication>
    {
        public void Configure(EntityTypeBuilder<Indication> builder)
        {
            DateTime DatTimeNow = DateTime.Now;

            builder.HasData(
                new Indication
                {
                    Id = 1,
                    SensorId = 1,
                    Temperature = 15,
                    CreateDate = DatTimeNow.AddHours(-16),
                    CreateTime = DatTimeNow.AddHours(-16).AddTicks(-(DatTimeNow.Ticks % 10000000)).TimeOfDay
                },
                new Indication
                {
                    Id = 2,
                    SensorId = 1,
                    Temperature = 20,
                    CreateDate = DatTimeNow.AddHours(-8),
                    CreateTime = DatTimeNow.AddHours(-8).AddTicks(-(DatTimeNow.Ticks % 10000000)).TimeOfDay
                },
                new Indication
                {
                    Id = 3,
                    SensorId = 1,
                    Temperature = 13,
                    CreateDate = DatTimeNow,
                    CreateTime = DatTimeNow.AddTicks(-(DatTimeNow.Ticks % 10000000)).TimeOfDay
                },
                new Indication
                {
                    Id = 4,
                    SensorId = 2,
                    Temperature = 13,
                    CreateDate = DatTimeNow.AddHours(-16),
                    CreateTime = DatTimeNow.AddHours(-16).AddTicks(-(DatTimeNow.Ticks % 10000000)).TimeOfDay
                },
                new Indication
                {
                    Id = 5,
                    SensorId = 2,
                    Temperature = 18,
                    CreateDate = DatTimeNow.AddHours(-8),
                    CreateTime = DatTimeNow.AddHours(-8).AddTicks(-(DatTimeNow.Ticks % 10000000)).TimeOfDay
                },
                new Indication
                {
                    Id = 6,
                    SensorId = 2,
                    Temperature = 14,
                    CreateDate = DatTimeNow,
                    CreateTime = DatTimeNow.AddTicks(-(DatTimeNow.Ticks % 10000000)).TimeOfDay
                },
                new Indication
                {
                    Id = 7,
                    SensorId = 3,
                    Temperature = 14,
                    CreateDate = DatTimeNow.AddHours(-16),
                    CreateTime = DatTimeNow.AddHours(-16).AddTicks(-(DatTimeNow.Ticks % 10000000)).TimeOfDay
                },
                new Indication
                {
                    Id = 8,
                    SensorId = 3,
                    Temperature = 19,
                    CreateDate = DatTimeNow.AddHours(-8),
                    CreateTime = DatTimeNow.AddHours(-8).AddTicks(-(DatTimeNow.Ticks % 10000000)).TimeOfDay
                },
                new Indication
                {
                    Id = 9,
                    SensorId = 3,
                    Temperature = 14,
                    CreateDate = DatTimeNow,
                    CreateTime = DatTimeNow.AddTicks(-(DatTimeNow.Ticks % 10000000)).TimeOfDay
                });
        }
    }
}

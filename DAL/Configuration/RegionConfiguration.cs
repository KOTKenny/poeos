﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Configuration
{
    public class RegionConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder) =>
            builder.HasData(
                new Region
                {
                    Id = 1,
                    Name = "Гродно"
                },
                new Region
                {
                    Id = 2,
                    Name = "Минск"
                });
    }
}

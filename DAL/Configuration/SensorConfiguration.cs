﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Configuration
{
    public class SensorConfiguration : IEntityTypeConfiguration<Sensor>
    {
        public void Configure(EntityTypeBuilder<Sensor> builder)
        {
            builder.HasData(
                new Sensor
                {
                    Id = 1,
                    Name = "Датчик 1",
                    RegionId = 1,
                    Latitude = 200,
                    Longitude = 250
                },
                new Sensor
                {
                    Id = 2,
                    Name = "Датчик 2",
                    RegionId = 2,
                    Latitude = 100,
                    Longitude = 150
                },
                new Sensor
                {
                    Id = 3,
                    Name = "Датчик 3",
                    RegionId = 2,
                    Latitude = 220,
                    Longitude = 480
                }
            );
        }
    }
}

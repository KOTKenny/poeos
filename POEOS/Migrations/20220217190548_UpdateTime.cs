﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace POEOS.Migrations
{
    public partial class UpdateTime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(219480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(507480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(795480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(219480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(507480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(795480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(219480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(507480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(795480006209) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 5, 38, 57, 641, DateTimeKind.Local).AddTicks(9569), new TimeSpan(203376434617) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 13, 38, 57, 643, DateTimeKind.Local).AddTicks(5132), new TimeSpan(491376435139) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 21, 38, 57, 643, DateTimeKind.Local).AddTicks(5142), new TimeSpan(779376435144) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 5, 38, 57, 643, DateTimeKind.Local).AddTicks(5145), new TimeSpan(203376435147) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 13, 38, 57, 643, DateTimeKind.Local).AddTicks(5149), new TimeSpan(491376435151) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 21, 38, 57, 643, DateTimeKind.Local).AddTicks(5153), new TimeSpan(779376435154) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 5, 38, 57, 643, DateTimeKind.Local).AddTicks(5156), new TimeSpan(203376435157) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 13, 38, 57, 643, DateTimeKind.Local).AddTicks(5159), new TimeSpan(491376435160) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 21, 38, 57, 643, DateTimeKind.Local).AddTicks(5162), new TimeSpan(779376435163) });
        }
    }
}

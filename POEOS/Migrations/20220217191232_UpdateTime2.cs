﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace POEOS.Migrations
{
    public partial class UpdateTime2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(287999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(575999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(863999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(287999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(575999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(863999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(287999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(575999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(863999999271) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(219480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(507480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(795480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(219480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(507480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(795480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(219480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(507480006209) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 5, 48, 234, DateTimeKind.Local).AddTicks(6209), new TimeSpan(795480006209) });
        }
    }
}

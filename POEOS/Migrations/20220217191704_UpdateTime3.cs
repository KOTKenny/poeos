﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace POEOS.Migrations
{
    public partial class UpdateTime3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 6, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 14, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 22, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 6, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 14, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(802238801946) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 6, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 14, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 22, 17, 3, 0) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(287999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(575999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(863999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(287999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(575999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(863999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(287999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(575999999271) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 12, 31, 913, DateTimeKind.Local).AddTicks(9271), new TimeSpan(863999999271) });
        }
    }
}

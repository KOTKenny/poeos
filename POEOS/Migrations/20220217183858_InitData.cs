﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace POEOS.Migrations
{
    public partial class InitData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Indices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SensorId = table.Column<int>(type: "int", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "Date", nullable: false),
                    CreateTime = table.Column<TimeSpan>(type: "time", nullable: false),
                    Temperature = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Indices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Regions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sensors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RegionId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Longitude = table.Column<int>(type: "int", nullable: false),
                    Latitude = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sensors", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Indices",
                columns: new[] { "Id", "CreateDate", "CreateTime", "SensorId", "Temperature" },
                values: new object[,]
                {
                    { 1, new DateTime(2022, 2, 17, 5, 38, 57, 641, DateTimeKind.Local).AddTicks(9569), new TimeSpan(203376434617), 1, 15 },
                    { 2, new DateTime(2022, 2, 17, 13, 38, 57, 643, DateTimeKind.Local).AddTicks(5132), new TimeSpan(491376435139), 1, 20 },
                    { 3, new DateTime(2022, 2, 17, 21, 38, 57, 643, DateTimeKind.Local).AddTicks(5142), new TimeSpan(779376435144), 1, 13 },
                    { 4, new DateTime(2022, 2, 17, 5, 38, 57, 643, DateTimeKind.Local).AddTicks(5145), new TimeSpan(203376435147), 2, 13 },
                    { 5, new DateTime(2022, 2, 17, 13, 38, 57, 643, DateTimeKind.Local).AddTicks(5149), new TimeSpan(491376435151), 2, 18 },
                    { 6, new DateTime(2022, 2, 17, 21, 38, 57, 643, DateTimeKind.Local).AddTicks(5153), new TimeSpan(779376435154), 2, 14 },
                    { 7, new DateTime(2022, 2, 17, 5, 38, 57, 643, DateTimeKind.Local).AddTicks(5156), new TimeSpan(203376435157), 3, 14 },
                    { 8, new DateTime(2022, 2, 17, 13, 38, 57, 643, DateTimeKind.Local).AddTicks(5159), new TimeSpan(491376435160), 3, 19 },
                    { 9, new DateTime(2022, 2, 17, 21, 38, 57, 643, DateTimeKind.Local).AddTicks(5162), new TimeSpan(779376435163), 3, 14 }
                });

            migrationBuilder.InsertData(
                table: "Regions",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Гродно" },
                    { 2, "Минск" }
                });

            migrationBuilder.InsertData(
                table: "Sensors",
                columns: new[] { "Id", "Latitude", "Longitude", "Name", "RegionId" },
                values: new object[,]
                {
                    { 1, 200, 250, "Датчик 1", 1 },
                    { 2, 100, 150, "Датчик 2", 2 },
                    { 3, 220, 480, "Датчик 3", 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Indices");

            migrationBuilder.DropTable(
                name: "Regions");

            migrationBuilder.DropTable(
                name: "Sensors");
        }
    }
}

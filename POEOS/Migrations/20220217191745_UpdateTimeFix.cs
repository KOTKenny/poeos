﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace POEOS.Migrations
{
    public partial class UpdateTimeFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 17, 44, 710, DateTimeKind.Local).AddTicks(4273), new TimeSpan(0, 6, 17, 44, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 17, 44, 710, DateTimeKind.Local).AddTicks(4273), new TimeSpan(0, 14, 17, 44, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 17, 44, 710, DateTimeKind.Local).AddTicks(4273), new TimeSpan(0, 22, 17, 44, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 17, 44, 710, DateTimeKind.Local).AddTicks(4273), new TimeSpan(0, 6, 17, 44, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 17, 44, 710, DateTimeKind.Local).AddTicks(4273), new TimeSpan(0, 14, 17, 44, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 17, 44, 710, DateTimeKind.Local).AddTicks(4273), new TimeSpan(0, 22, 17, 44, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 17, 44, 710, DateTimeKind.Local).AddTicks(4273), new TimeSpan(0, 6, 17, 44, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 17, 44, 710, DateTimeKind.Local).AddTicks(4273), new TimeSpan(0, 14, 17, 44, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 17, 44, 710, DateTimeKind.Local).AddTicks(4273), new TimeSpan(0, 22, 17, 44, 0) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 6, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 14, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 22, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 6, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 14, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(802238801946) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 6, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 6, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 14, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 14, 17, 3, 0) });

            migrationBuilder.UpdateData(
                table: "Indices",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreateDate", "CreateTime" },
                values: new object[] { new DateTime(2022, 2, 17, 22, 17, 3, 880, DateTimeKind.Local).AddTicks(1946), new TimeSpan(0, 22, 17, 3, 0) });
        }
    }
}

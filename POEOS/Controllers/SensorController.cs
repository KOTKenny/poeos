﻿using DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace POEOS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SensorController : ControllerBase
    {
        private IRepositoryManager _repository;
        public SensorController(IRepositoryManager repositoryManager)
        {
            _repository = repositoryManager;
        }

        [HttpGet, Authorize]
        public IActionResult GetAllSensors()
        {
            var sensors = _repository.SensorRepository.GetAllSensors(false).ToList();

            if (sensors == null)
                return NotFound();

            for (int i = 0; i < sensors.Count; i++)
            {
                sensors[i].Region = _repository.RegionRepository.GetRegionById(sensors[i].RegionId, false);
            }

            return Ok(sensors);
        }

        [HttpGet("{id}"), Authorize]
        public IActionResult GetSensorById(int id)
        {
            var sensor = _repository.SensorRepository.GetSensorById(id, false);

            if (sensor == null)
                return NotFound();

            return Ok(sensor);
        }
    }
}

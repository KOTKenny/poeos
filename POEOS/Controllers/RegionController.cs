﻿using DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace POEOS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegionController : ControllerBase
    {
        private IRepositoryManager _repository;
        public RegionController(IRepositoryManager repositoryManager)
        {
            _repository = repositoryManager;
        }

        [HttpGet, Authorize]
        public IActionResult GetAllRegions()
        {
            var regions = _repository.RegionRepository.GetAllRegions(false);

            if (regions == null)
                return NotFound();

            return Ok(regions);
        }

        [HttpGet("{id}"), Authorize]
        public IActionResult GetRegionById(int id)
        {
            var region = _repository.RegionRepository.GetRegionById(id, false);

            if (region == null)
                return NotFound();

            return Ok(region);
        }
    }
}

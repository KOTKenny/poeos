﻿using AutoMapper;
using DAL;
using DAL.Models;
using DAL.Models.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace POEOS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IndicationController : ControllerBase
    {
        private IRepositoryManager _repository;
        private IMapper _mapper;
        public IndicationController(IRepositoryManager repositoryManager, IMapper mapper)
        {
            _repository = repositoryManager;
            _mapper = mapper;
        }

        [HttpGet, Authorize]
        public IActionResult GetAllIndications()
        {
            var indications = _repository.IndicationRepository.GetAllIndications(false).ToList();

            if (indications == null)
                return NotFound();

            for (int i = 0; i < indications.Count; i++)
            {
                indications[i].Sensor = _repository.SensorRepository.GetSensorById(indications[i].SensorId, false);
                indications[i].Sensor.Region = _repository.RegionRepository.GetRegionById(indications[i].Sensor.RegionId, false);
            }

            var indicationsDto = _mapper.Map<List<Indication>, List<IndicationDTO>>(indications);

            return Ok(indicationsDto);
        }

        [HttpGet("{id}"), Authorize]
        public IActionResult GetIndicationById(int id)
        {
            var indication = _repository.IndicationRepository.GetIndicationById(id, false);

            if (indication == null)
                return NotFound();

            return Ok(indication);
        }
    }
}

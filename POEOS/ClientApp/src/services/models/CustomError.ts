export interface CustomError{
    statusCode: string
    message: string
}
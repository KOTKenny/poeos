export const Authentication = (email: string, password: string) => {
    return fetch('api/authentication/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => {
            console.log(res.status)
            if (res.status == 200)
                return res.json()
            else
                return "";
        })
        .then(res => {
            if(res != "")
                return res.token as string;
            else
                return "";
        })
}

export const SetJWT = (token: string) => {
    if(token != "")
        localStorage.setItem('token', token);
}

export const LoggedIn = () : boolean => {
    return localStorage.getItem('token') != null
}

export const GetJWT = () : string | null => {
    var token = localStorage.getItem('token');

    return token != null ? token : null;
}

export const LogOut = () => {
    localStorage.removeItem('token');
}
import React, { useState, useEffect } from 'react';
import { Table, Spinner } from 'reactstrap'
import { GetJWT } from '../services/authService';

export const Regions = () => {
  const [regions, setRegions] = useState(null);

  useEffect(() => {
    fetch('api/region', {
      headers:{
        'Authorization' : 'Bearer ' + GetJWT()
      }
    })
      .then(res => res.json())
      .then(res => {
        setRegions(res);
      })
  }, [])


  if (regions == null)
    return (
      <div className="d-flex justify-content-center">
        <Spinner
          color="dark"
          style={{ width: 50, height: 50 }}
        >
          Loading...
        </Spinner>
      </div>
    );
  else
    return (
      <Table striped>
        <thead>
          <tr>
            <th>
              #
            </th>
            <th>
              Наименование
            </th>
          </tr>
        </thead>
        <tbody>
          {regions.map((region) =>
            <tr>
              <th scope="row">
                {region.id}
              </th>
              <td>
                {region.name}
              </td>
            </tr>)}
        </tbody>
      </Table>
    );
}

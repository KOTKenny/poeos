import React, { useState, useEffect } from 'react';
import { Form, NavbarBrand, FormGroup, Label, Input, Button, Navbar, Container } from 'reactstrap'
import { Authentication, LoggedIn, SetJWT } from '../services/authService';

export const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <div>
      <Navbar className="navbar-expand-sm navbar-toggleable-sm navbar navbar-dark bg-dark border-bottom box-shadow mb-3" light>
        <Container>
          <NavbarBrand to="/">POEOS</NavbarBrand>
        </Container>
      </Navbar>
      <Container>
        <FormGroup className="mb-2">
          <Label
            className="me-sm-2"
            for="exampleEmail"
          >
            Email
          </Label>
          <Input
            id="exampleEmail"
            name="email"
            placeholder="Email"
            type="email"
            onChange={(e) => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup className="mb-2">
          <Label
            className="me-sm-2"
            for="examplePassword"
          >
            Пароль
          </Label>
          <Input
            id="examplePassword"
            name="password"
            placeholder="Пароль"
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </FormGroup>
        <Button onClick={() => {
          LogIn(email, password);
        }} className="w-100">
          Войти
        </Button>
      </Container>
    </div>
  );
}

function LogIn(email: string, password: string) {
  if (email != "" && password != "" && !LoggedIn()) {
    Authentication(email, password).then(res => {
      SetJWT(res);
      if (res != "")
        window.location.replace("/");
    });
  }
}

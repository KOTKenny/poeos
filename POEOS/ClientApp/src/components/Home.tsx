import React, { useState, useEffect } from 'react';
import { Table, Spinner, Container } from 'reactstrap'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { GetJWT } from '../services/authService';

interface ChartData {
  createDate: string,
  sensor1: string,
  sensor2: string,
  sensor3: string,
}

const chartData = new Array<ChartData>();

export const Home = () => {
  const [indications, setIndications] = useState(null);

  useEffect(() => {
    fetch('api/indication', {
      headers:{
        'Authorization' : 'Bearer ' + GetJWT()
      }
    })
      .then(res => res.json())
      .then(res => {
        res.forEach(element => {
          var chartIndex = chartData.findIndex(x => x.createDate == element.createDate);
          if (chartIndex == -1) {
            chartData.push({
              createDate: element.createDate,
              sensor1: element.sensor.name == "Датчик 1" ? element.temperature : "",
              sensor2: element.sensor.name == "Датчик 2" ? element.temperature : "",
              sensor3: element.sensor.name == "Датчик 3" ? element.temperature : ""
            })
          } else {
            if (element.sensor.name == "Датчик 1")
              chartData[chartIndex].sensor1 = element.temperature
            else if (element.sensor.name == "Датчик 2")
              chartData[chartIndex].sensor2 = element.temperature
            else
              chartData[chartIndex].sensor3 = element.temperature
          }
        });
        console.log(chartData);
        setIndications(res);
      })
  }, [])


  if (indications == null)
    return (
      <div className="d-flex justify-content-center">
        <Spinner
          color="dark"
          style={{ width: 50, height: 50 }}
        >
          Loading...
        </Spinner>
      </div>
    );
  else
    return (
      <Container className="d-flex flex-column justify-content-center">
        <LineChart
          width={500}
          height={300}
          data={chartData}
          className="align-self-center"
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="createDate" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="sensor1" stroke="#8884d8" />
          <Line type="monotone" dataKey="sensor2" stroke="#82ca9d" />
          <Line type="monotone" dataKey="sensor3" stroke="#149691" />
        </LineChart>
        <Table striped>
          <thead>
            <tr>
              <th>
                #
              </th>
              <th>
                Температура
              </th>
              <th>
                Дата создания
              </th>
              <th>
                Датчик
              </th>
              <th>
                Регион
              </th>
            </tr>
          </thead>
          <tbody>
            {indications.map((indication) =>
              <tr>
                <th scope="row">
                  {indication.id}
                </th>
                <td>
                  {indication.temperature}
                </td>
                <td>
                  {indication.createDate}
                </td>
                <td>
                  {indication.sensor.name}
                </td>
                <td>
                  {indication.sensor.region.name}
                </td>
              </tr>)}
          </tbody>
        </Table>
      </Container>
    );
}

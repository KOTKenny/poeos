import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import { LogOut } from '../services/authService';
import './NavMenu.css';

export class NavMenu extends Component {
  static displayName = NavMenu.name;

  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render () {
    return (
      <header>
        <Navbar className="navbar-expand-sm navbar-toggleable-sm navbar navbar-dark bg-dark border-bottom box-shadow mb-3" light>
          <Container>
            <NavbarBrand tag={Link} to="/">POEOS</NavbarBrand>
            <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
            <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
              <ul className="navbar-nav flex-grow">
                <NavItem>
                  <NavLink tag={Link} to="/">Главная</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} to="/sensors">Датчки</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={Link} to="/regions">Регионы</NavLink>
                </NavItem>
                <Button onClick={() => {
                    LogOut();
                    window.location.replace("/");
                  }}>
                  Выйти
                </Button>
              </ul>
            </Collapse>
          </Container>
        </Navbar>
      </header>
    );
  }
}

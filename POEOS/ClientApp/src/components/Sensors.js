import React, { useState, useEffect } from 'react';
import { Table, Spinner } from 'reactstrap'
import { GetJWT } from '../services/authService';

export const Sensors = () => {
  const [sensors, setSensors] = useState(null);

  useEffect(() => {
    fetch('api/sensor', {
      headers: {
        'Authorization': 'Bearer ' + GetJWT()
      }
    })
      .then(res => res.json())
      .then(res => {
        setSensors(res);
      })
  }, [])


  if (sensors == null)
    return (
      <div className="d-flex justify-content-center">
        <Spinner
          color="dark"
          style={{ width: 50, height: 50 }}
        >
          Loading...
        </Spinner>
      </div>
    );
  else
    return (
      <Table striped>
        <thead>
          <tr>
            <th>
              #
            </th>
            <th>
              Наименование
            </th>
            <th>
              Долгота
            </th>
            <th>
              Широта
            </th>
            <th>
              Регион
            </th>
          </tr>
        </thead>
        <tbody>
          {sensors.map((sensor) =>
            <tr>
              <th scope="row">
                {sensor.id}
              </th>
              <td>
                {sensor.name}
              </td>
              <td>
                {sensor.longitude}
              </td>
              <td>
                {sensor.latitude}
              </td>
              <td>
                {sensor.region.name}
              </td>
            </tr>)}
        </tbody>
      </Table>
    );
}

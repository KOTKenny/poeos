import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { Sensors } from './components/Sensors';

import './custom.css'
import { Regions } from './components/Regions';
import { Login } from './components/Login';
import { LoggedIn } from './services/authService';

export default class App extends Component {
  static displayName = App.name;

  render() {
    if (LoggedIn())
      return (
        <Layout>
          <Route exact path='/' component={Home} />
          <Route path='/sensors' component={Sensors} />
          <Route path='/regions' component={Regions} />
        </Layout>
      );
    else
      return (
          <Login />
      );
  }
}

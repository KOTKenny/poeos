﻿using AutoMapper;
using BLL;
using DAL.Models;
using DAL.Models.DTO;
using Microsoft.AspNetCore.Identity;

namespace POEOS
{
    public class MapperProfile : Profile
    {
        internal MapperProfile()
        {
            CreateMap<Indication, IndicationDTO>()
                .ForPath(x => x.CreateDate, opt => 
                    opt.MapFrom(src => Converter.ConvertDateAndTime(src.CreateDate, src.CreateTime)));

            CreateMap<LoginModel, IdentityUser>();
        }
    }
}

﻿using System;

namespace BLL
{
    public static class Converter
    {
        public static DateTime ConvertDateAndTime(DateTime createDate, TimeSpan createTime) =>
            createDate.AddHours(createTime.Hours).AddMinutes(createTime.Minutes).AddSeconds(createTime.Seconds);
    }
}
